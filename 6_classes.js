class Book{
	constructor(title, author, year){
		this.title=title;
		this.author=author;
		this.year=year;
	}
	getSummary(){
		return this.title;
	}
	getAge() {
		const years = new Date().getFullYear()-this.year;
		return years;
	}
	revise(newYear){
		this.year= newYear;
		this.revised=true;
	}
	static topBookStore(){
		return "Barnes & Nobel";
	}
}

//instantiate

const book1 = new Book("book1","john doe","2016");
console.log(book1);
book1.revise(2019);
console.log(book1);
console.log(Book.topBookStore());