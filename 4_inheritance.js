// constructor 
function Book(title,author,year){
	this.title=title;
	this.author=author;
	this.year=year;
	/*this.getSummary = function(){
		return this.title;
	}*/
}
//getSummary
Book.prototype.getSummary=function(){
	return this.author;
};

function Magazine(title, author, year, month){
	Book.call(this, title, author, year);
	this.month=month;
}

//Inherit Prototype
Magazine.prototype = Object.create(Book.prototype);

//instantiate magazine object
const mag1=new Magazine("Mag1", "john Doe", "2018", "jan");

//use magazine constructor
Magazine.prototype.constructor=Magazine;

console.log(mag1.getSummary());
console.log(mag1);