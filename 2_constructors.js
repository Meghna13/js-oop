// constructor 
function Book(title,author,year){
	this.title=title;
	this.author=author;
	this.year=year;
	this.getSummary = function(){
		return this.title;
	}

}

//instantiation of an object
const Book1 = new Book("book1","jon Doe", "2013");
const Book2 = new Book("book2","jane doe","2018");

console.log(Book1.title);
console.log(Book2.title);
console.log(Book1.getSummary());

