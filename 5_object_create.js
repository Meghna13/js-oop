//object of protos
const bookProtos = {
	getSummary:function(){
		return this.title;
	},
	getAge:function(){
		return this.age;
	}
}

//create object 

/*const book1= Object.create(bookProtos);
book1.title="book1";
book1.author="john doe";
book1.year="2018";
console.log(book1);
*/

const book1= Object.create(bookProtos,{
	title:{value: "book1"},
	author:{value: "john doe"},
	year:{value: "2018"}

});

console.log(book1);
