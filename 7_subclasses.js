class Book{
	constructor(title, author, year){
		this.title=title;
		this.author=author;
		this.year=year;
	}
	getSummary(){
		return this.title;
	}
}

//magazine subclass

class Magazine extends Book {
	constructor(title, author, year, month){
		super(title, author, year);
		this.month=month;
	}
}

//instantiate

const book1 = new Book("book1","john doe","2016");
console.log(book1);
//book1.revise(2019);
console.log(book1);
//console.log(Book.topBookStore());

//instantiate

const mag1 = new Magazine("mag1", "jd", "2017", "jan");
console.log(mag1);
console.log(mag1.getSummary());