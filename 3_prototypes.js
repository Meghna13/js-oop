// constructor 
function Book(title,author,year){
	this.title=title;
	this.author=author;
	this.year=year;
	/*this.getSummary = function(){
		return this.title;
	}*/
}
//getSummary
Book.prototype.getSummary=function(){
	return this.author;
}
//getAge
Book.prototype.getAge=function(){
	const years = new Date().getFullYear()-this.year;
	return years;
}
//Revise
Book.prototype.revise = function(newYear){
	this.year= newYear;
	this.revised=true;
}
//instantiation of an object
const Book1 = new Book("book1","jon Doe", "2013");
const Book2 = new Book("book2","jane doe","2018");
//console.log(Book1.title);
//console.log(Book2.title);
console.log(Book2.getSummary());
console.log(Book2);
console.log(Book1.getAge());
console.log(Book1);
Book1.revise('2019');
console.log(Book1);


